package face

import (
	"encoding/json"
	"errors"

	"github.com/astaxie/beego/httplib"
)

type FaceClient struct {
	apiKey    string
	apiSecret string
}

func NewFaceClient(apiKey, apiSecret string) *FaceClient {
	return &FaceClient{
		apiKey:    apiKey,
		apiSecret: apiSecret,
	}
}

func (c *FaceClient) RecognizeIdentityLicence(base64 string) (*IdentityLicence, error) {
	hc := httplib.Post("https://api-cn.faceplusplus.com/cardpp/v1/ocridcard")
	hc = hc.Param("api_key", c.apiKey)
	hc = hc.Param("api_secret", c.apiSecret)
	hc = hc.Param("image_base64", base64)

	body, err := hc.Bytes()
	if err != nil {
		return nil, err
	}

	var rsp identifyCardRsp
	err = json.Unmarshal(body, &rsp)
	if err != nil {
		return nil, err
	}

	if len(rsp.Cards) == 0 {
		return nil, errors.New("no card info")
	}

	card := rsp.Cards[0]
	ident := new(IdentityLicence)
	ident.Idno = card.IDCardNumber
	ident.Address = card.Address
	ident.Name = card.Name
	ident.Gender = card.Gender
	ident.Birthday = card.Birthday
	return ident, nil
}

func (c *FaceClient) RecognizeDrivingLicence(base64 string) (*DrivingLicence, error) {
	hc := httplib.Post("https://api-cn.faceplusplus.com/cardpp/v2/ocrdriverlicense")
	hc = hc.Param("api_key", c.apiKey)
	hc = hc.Param("api_secret", c.apiSecret)
	hc = hc.Param("image_base64", base64)

	body, err := hc.Bytes()
	if err != nil {
		return nil, err
	}

	var rsp drvingLicenceRsp
	err = json.Unmarshal(body, &rsp)
	if err != nil {
		return nil, err
	}

	driverLicence := new(DrivingLicence)

	if len(rsp.Main) > 0 {
		front := rsp.Main[0]
		driverLicence.Address = front.Address.Content
		driverLicence.Idno = front.LicenseNumber.Content
		driverLicence.Register = front.IssueDate.Content
		driverLicence.Name = front.Name.Content
		driverLicence.Gender = front.Gender.Content
		driverLicence.Birthday = front.Birthday.Content
		driverLicence.Class = front.Class.Content
		driverLicence.IssuedBy = front.IssuedBy.Content
		driverLicence.IssuedDate = front.IssueDate.Content
		driverLicence.Nationality = front.Nationality.Content
		driverLicence.ValidFrom = front.ValidFrom.Content
		driverLicence.ValidFor = front.ValidFor.Content
	}

	if len(rsp.Second) > 0 {

	}

	return driverLicence, nil
}

func (c *FaceClient) RecognizeVehicleLicence(base64 string) (*VehicleLicence, error) {
	hc := httplib.Post("https://api-cn.faceplusplus.com/cardpp/v1/ocrvehiclelicense")
	hc = hc.Param("api_key", c.apiKey)
	hc = hc.Param("api_secret", c.apiSecret)
	hc = hc.Param("image_base64", base64)
	body, err := hc.Bytes()
	if err != nil {
		return nil, err
	}

	var rsp vehicleLicenceRsp
	err = json.Unmarshal(body, &rsp)
	if err != nil {
		return nil, err
	}

	if len(rsp.Cards) == 0 {
		return nil, errors.New("no card info")
	}
	card := rsp.Cards[0]

	vehicle := new(VehicleLicence)
	vehicle.Address = card.Address
	vehicle.Register = card.RegisterDate
	vehicle.Plate = card.PlateNo
	vehicle.Vin = card.Vin
	vehicle.Engine = card.EngineNo
	vehicle.Owner = card.Owner
	return vehicle, nil
}
