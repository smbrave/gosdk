package face

//驾驶证
type DrivingLicence struct {
	Idno        string `json:"idno,omitempty"`
	Name        string `json:"name,omitempty"`
	Address     string `json:"address,omitempty"`
	Register    string `json:"register,omitempty"`
	Gender      string `json:"gender,omitempty"`
	Birthday    string `json:"birthday,omitempty"`
	IssuedBy    string `json:"issued_by,omitempty"`
	IssuedDate  string `json:"issued_date,omitempty"`
	Nationality string `json:"nationality,omitempty"`
	ValidFrom   string `json:"valid_from,omitempty"`
	ValidFor    string `json:"valid_for,omitempty"`
	Class       string `json:"class,omitempty"`
}

//行驶证
type VehicleLicence struct {
	Plate    string `json:"plate,omitempty"`
	Vin      string `json:"vin,omitempty"`
	Engine   string `json:"engine,omitempty"`
	Register string `json:"register,omitempty"`
	Address  string `json:"address,omitempty"`
	Owner    string `json:"owner,omitempty"`
}

//身份证
type IdentityLicence struct {
	Idno     string `json:"idno,omitempty"`
	Name     string `json:"name,omitempty"`
	Address  string `json:"address,omitempty"`
	Birthday string `json:"birthday,omitempty"`
	Gender   string `json:"gender,omitempty"`
}
