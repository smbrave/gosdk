package face

//内部=========================
type baseRsp struct {
	ImageId   string `json:"image_id"`
	RequestId string `json:"request_id"`
}

type identifyCardRsp struct {
	baseRsp
	Cards []*identifyCard `json:"cards"`
}

type identifyCard struct {
	Address      string `json:"address"`
	Birthday     string `json:"birthday"`
	Gender       string `json:"gender"`
	IDCardNumber string `json:"id_card_number"`
	IssuedBy     string `json:"issued_by"`
	Name         string `json:"name"`
	Race         string `json:"race"`
	Side         string `json:"side"`
	Type         int    `json:"type"`
	ValidDate    string `json:"valid_date"`
}

type vehicleLicenceRsp struct {
	baseRsp
	Cards []*vehilceLicence `json:"cards"`
}

type vehilceLicence struct {
	Address      string `json:"address"`
	EngineNo     string `json:"engine_no"`
	IssueDate    string `json:"issue_date"`
	IssuedBy     string `json:"issued_by"`
	Model        string `json:"model"`
	Owner        string `json:"owner"`
	PlateNo      string `json:"plate_no"`
	RegisterDate string `json:"register_date"`
	Side         string `json:"side"`
	Type         int    `json:"type"`
	UseCharacter string `json:"use_character"`
	VehicleType  string `json:"vehicle_type"`
	Vin          string `json:"vin"`
}

type drvingLicenceRsp struct {
	baseRsp
	Main   []*drvingLicenceMain   `json:"main"`
	Second []*drvingLicenceSecond `json:"second"`
}

type drvingLicenceMain struct {
	Address struct {
		Content string `json:"content"`
	} `json:"address"`
	Birthday struct {
		Content string `json:"content"`
	} `json:"birthday"`
	Class struct {
		Content string `json:"content"`
	} `json:"class"`
	Gender struct {
		Content string `json:"content"`
	} `json:"gender"`
	IssueDate struct {
		Content string `json:"content"`
	} `json:"issue_date"`
	IssuedBy struct {
		Content string `json:"content"`
	} `json:"issued_by"`
	LicenseNumber struct {
		Content string `json:"content"`
	} `json:"license_number"`
	Name struct {
		Content string `json:"content"`
	} `json:"name"`
	Nationality struct {
		Content string `json:"content"`
	} `json:"nationality"`
	ValidDate struct {
		Content string `json:"content"`
	} `json:"valid_date"`
	ValidFor struct {
		Content string `json:"content"`
	} `json:"valid_for"`
	ValidFrom struct {
		Content string `json:"content"`
	} `json:"valid_from"`
	Version struct {
		Content int `json:"content"`
	} `json:"version"`
}

type drvingLicenceSecond struct {
	FileNumber struct {
		Content string `json:"content"`
	} `json:"file_number"`
	LicenseNumber struct {
		Content string `json:"content"`
	} `json:"license_number"`
	Name struct {
		Content string `json:"content"`
	} `json:"name"`
}
