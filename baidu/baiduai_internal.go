package baidu

type ai_token_t struct {
	AccessToken   string `json:"access_token"`
	ExpiresIn     int    `json:"expires_in"`
	RefreshToken  string `json:"refresh_token"`
	Scope         string `json:"scope"`
	SessionKey    string `json:"session_key"`
	SessionSecret string `json:"session_secret"`
}

type ai_base_response_t struct {
	Errno int    `json:"errno"`
	Msg   string `json:"msg"`
}

type ai_vehilce_licence_t struct {
	Address struct {
		Words string `json:"words"`
	} `json:"住址"`

	Usage struct {
		Words string `json:"words"`
	} `json:"使用性质"`
	Engine struct {
		Words string `json:"words"`
	} `json:"发动机号码"`
	IssueDate struct {
		Words string `json:"words"`
	} `json:"发证日期"`
	Plate struct {
		Words string `json:"words"`
	} `json:"号牌号码"`
	Bran struct {
		Words string `json:"words"`
	} `json:"品牌型号"`
	Owner struct {
		Words string `json:"words"`
	} `json:"所有人"`
	RegisteDate struct {
		Words string `json:"words"`
	} `json:"注册日期"`
	Class struct {
		Words string `json:"words"`
	} `json:"车辆类型"`
	Vin struct {
		Words string `json:"words"`
	} `json:"车辆识别代号"`
}

type ai_vehicle_licence_rsp_t struct {
	ai_base_response_t
	WordsResultNum int                  `json:"words_result_num"`
	Data           ai_vehilce_licence_t `json:"words_result"`
}

type ai_driving_licence_t struct {
	Address struct {
		Words string `json:"words"`
	} `json:"住址"`
	Class struct {
		Words string `json:"words"`
	} `json:"准驾车型"`
	Birthday struct {
		Words string `json:"words"`
	} `json:"出生日期"`
	FirstDate struct {
		Words string `json:"words"`
	} `json:"初次领证日期"`
	Contry struct {
		Words string `json:"words"`
	} `json:"国籍"`
	Name struct {
		Words string `json:"words"`
	} `json:"姓名"`
	Gender struct {
		Words string `json:"words"`
	} `json:"性别"`
	ValidFor struct {
		Words string `json:"words"`
	} `json:"有效期限"`
	ValidFrom struct {
		Words string `json:"words"`
	} `json:"有效起始日期"`
	Idno struct {
		Words string `json:"words"`
	} `json:"证号"`
}
type ai_driving_licence_rsp_t struct {
	ai_base_response_t

	WordsResult    ai_driving_licence_t `json:"words_result"`
	WordsResultNum int                  `json:"words_result_num"`
}
