package baidu

import (
	"fmt"
	"time"

	"github.com/astaxie/beego/httplib"
)

type BaiduAiClient struct {
	apiKey    string
	secretKey string
	token     *ai_token_t
}

func NewBaiduAiClient(apiKey, secretKey string) (*BaiduAiClient, error) {
	c := &BaiduAiClient{
		apiKey:    apiKey,
		secretKey: secretKey,
	}
	err := c.updateToken()
	if err != nil {
		return nil, err
	}
	go c.loopToken()
	return c, nil
}

func (c *BaiduAiClient) loopToken() error {
	timer := time.NewTicker(24 * time.Hour)
	for {
		select {
		case <-timer.C:
			c.updateToken()
		}
	}
}

func (c *BaiduAiClient) updateToken() error {
	hc := httplib.Get("https://aip.baidubce.com/oauth/2.0/token")
	hc.Param("grant_type", "client_credentials")
	hc.Param("client_id", c.apiKey)
	hc.Param("client_secret", c.secretKey)

	token := new(ai_token_t)
	err := hc.ToJSON(token)
	if err != nil {
		return err
	}
	c.token = token
	return nil
}

func (c *BaiduAiClient) RecognizeVehicleLicence(image string) (*VehicleLicence, error) {
	hc := httplib.Post("https://aip.baidubce.com/rest/2.0/ocr/v1/vehicle_license")
	hc.Param("access_token", c.token.AccessToken)
	hc.Header("Content-Type", "application/x-www-form-urlencoded")

	hc.Param("image", image)

	var rsp ai_vehicle_licence_rsp_t
	err := hc.ToJSON(&rsp)
	if err != nil {
		return nil, err
	}
	if rsp.Errno != 0 {
		return nil, fmt.Errorf("errno:%d errms:%s", rsp.Errno, rsp.Msg)
	}
	result := new(VehicleLicence)
	result.Vin = rsp.Data.Vin.Words
	result.Engine = rsp.Data.Engine.Words
	result.Owner = rsp.Data.Owner.Words
	result.Plate = rsp.Data.Plate.Words
	result.Address = rsp.Data.Address.Words
	result.Register = rsp.Data.RegisteDate.Words

	return result, nil
}

func (c *BaiduAiClient) RecognizeDrivingLicence(image string) (*DrivingLicence, error) {
	hc := httplib.Post("https://aip.baidubce.com/rest/2.0/ocr/v1/driving_license")
	hc.Param("access_token", c.token.AccessToken)
	hc.Header("Content-Type", "application/x-www-form-urlencoded")

	hc.Param("image", image)

	var rsp ai_driving_licence_rsp_t

	err := hc.ToJSON(&rsp)
	if err != nil {
		return nil, err
	}

	if rsp.Errno != 0 {
		return nil, fmt.Errorf("errno:%d errms:%s", rsp.Errno, rsp.Msg)
	}

	result := new(DrivingLicence)
	result.Name = rsp.WordsResult.Name.Words
	result.Address = rsp.WordsResult.Address.Words
	result.Register = rsp.WordsResult.FirstDate.Words
	result.Nationality = rsp.WordsResult.Contry.Words
	result.ValidFor = rsp.WordsResult.ValidFor.Words
	result.ValidFrom = rsp.WordsResult.ValidFrom.Words
	result.IssuedDate = rsp.WordsResult.FirstDate.Words
	result.Class = rsp.WordsResult.Class.Words
	result.Birthday = rsp.WordsResult.Birthday.Words
	result.Gender = rsp.WordsResult.Gender.Words
	result.Idno = rsp.WordsResult.Idno.Words
	return result, nil
}
