package weixin

type weapp_template_data_t struct {
	Value string `json:"value"`
}

type weapp_template_msg_t struct {
	TemplateId      string                            `json:"template_id"`
	FormId          string                            `json:"form_id"`
	Page            string                            `json:"page,omitempty"`
	EmphasisKeyword string                            `json:"emphasis_keyword,omitempty"`
	Data            map[string]*weapp_template_data_t `json:"data"`
}

type weapp_mp_template_msg_t struct {
	TemplateId      string                            `json:"template_id"`
	Appid          string                            `json:"appid"`
	Url            string                            `json:"url,omitempty"`
	Miniprogram  struct {
		Appid string `json:"appid"`
		Pagepath string `json:"pagepath"`
	} `json:"miniprogram"`
	Data            map[string]*weapp_template_data_t `json:"data"`
}

type weapp_uniform_template_t struct {
	Touser          string          `json:"touser"`
	WeappMsg *weapp_template_msg_t `json:"weapp_template_msg,omitempty"`
	MpMsg *weapp_mp_template_msg_t `json:"mp_template_msg,omitempty"`
}