package weixin

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	math_rand "math/rand"
	"time"
)

func RandomStr(length int) string {
	str := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	bytes := []byte(str)
	result := []byte{}
	r := math_rand.New(math_rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < length; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}

// rsa加密
var (
	pcs8 string = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnSbydFlUqBrAIt67c9az
kFlMTpTEMvr/TQ6EGLeCgsVDSTVUs1nhKaXKgqachLNYxRZ5+UztuV7IFbLtmuag
+LRkwXbiR5rnigPMAboDtWpC9JuCSK8OFIXVFA2YEQYsSHYcCifvJdHDkZbYAniD
EPR6MaZLJe9pGHEKr+KADkFU7N0D7Q9lFQfA4auNps6uoRRTYkCeYB4FRlP2miLL
km7JrCudbYznDdOs4z/HsfwuU9TPCTrOffXSZcY8P+CLYVcyEiKpp5upCb/taDN/
xSFCSRoUHfJ+XaQ2+EWxZxF0mAveGtmUMdfpFTdq7EqJ+1RRCpdKGydvpoAqIZ+F
vwIDAQAB
-----END PUBLIC KEY-----`
)

func RsaEncrypt(originData, publicKey []byte) ([]byte, error) {
	publicKey = []byte(pcs8)
	block, _ := pem.Decode(publicKey) //将密钥解析成公钥实例
	if block == nil {
		return nil, errors.New("public key error")
	}
	pubInterface, err := x509.ParsePKIXPublicKey(block.Bytes) //解析pem.Decode（）返回的Block指针实例
	if err != nil {
		return nil, err
	}
	pub := pubInterface.(*rsa.PublicKey)
	return rsa.EncryptPKCS1v15(rand.Reader, pub, originData) //RSA算法加密
}
