package weixin

import "encoding/xml"

///============================weapp=======================

type WeappConfig struct {
	AppID     string
	AppSecret string
}

type WeappJsSession struct {
	OpenId     string `json:"openid"`
	SessionKey string `json:"session_key"`
	UnionId    string `json:"union_id"`
}

type WeappTempleteRequest struct {
	Touser     string   `json:"touser"`
	WeappTemplateMsg struct {
		TemplateId string   `json:"template_id"`
		FormId     string   `json:"form_id"`
		Page       string   `json:"page,omitempty"`
		Data       []string `json:"data"`
	} `json:"weapp_msg"`

	MpTemplateMsg struct {
		Appid string `json:"appid"`
		TemplateId string   `json:"template_id"`
		Url       string   `json:"url,omitempty"`
		Miniprogram struct {
			Appid string `json:"appid"`
			Pagepath string `json:"pagepath"`
		} `json:"miniprogram"`
		Data       []string `json:"data"`
	}`json:"mp_msg"`
}

type WeappCustomRequest struct {
	MsgType string `json:"msgtype"`
	Touser  string `json:"touser"`
	Text    struct {
		Content string `json:"content"`
	} `json:"text"`

	Image struct {
		MediaId string `json:"media_id"`
	} `json:"image"`
	Link struct {
		Title       string `json:"title"`
		Url         string `json:"url"`
		Description string `json:"description"`
		ThumbUrl    string `json:"thumb_url"`
	} `json:"linke"`

	Page struct {
		Title        string `json:"title"`
		PagePath     string `json:"pagepath"`
		ThumbMediaId string `json:"thumb_media_id"`
	} `json:"miniprogrampage"`
}

type WeappBaseResonse struct {
	ErrCode int    `json:"errcode"`
	ErrMsg  string `json:"errmsg"`
}

///============================qiye=======================
type QiyeConfig struct {
	Corpid string
	Secret string
	Sender string
}

type QiyeToken struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int    `json:"expires_in"`
}

type QiyeResponse struct {
	ErrCode int    `json:"errcode"`
	ErrMsg  string `json:"errmsg"`
}

type QiyeBaseRequest struct {
	ToUser  string `json:"touser"`
	ToParty string `json:"toparty"`
	ToTag   string `json:"totag"`
	MstType string `json:"msgtype"`
	AgentId string `json:"agentid"`
}

type QiyeTextRequest struct {
	QiyeBaseRequest
	Text struct {
		Content string `json:"content"`
	} `json:"text"`
}

type QiyeImageRequest struct {
	QiyeBaseRequest
	Image struct {
		MediaID string `json:"media_id"`
	} `json:"image"`
}

type QiyeCardBtn struct {
	Key         string `json:"key"`
	Name        string `json:"name"`
	ReplaceName string `json:"replace_name"`
	Color       string `json:"color"`
	IsBold      bool   `json:"is_bolad"`
}

type QiyeCardRequest struct {
	QiyeBaseRequest

	TaskCard struct {
		Url         string         `json:"url"`
		Title       string         `json:"title"`
		Description string         `json:"description"`
		TaskId      string         `json:"task_id"`
		Btn         []*QiyeCardBtn `json:"btn"`
	} `json:"taskcard"`
}

type QiyeUploadResponse struct {
	QiyeResponse
	MediaId  string `json:"media_id"`
	CreateAt string `json:"created_at"`
	Type     string `json:"type"`
}

///============================pay=======================

type PayConfig struct {
	AppID        string
	AppSecret    string
	PayMchID     string //支付 - 商户 ID
	PayNotifyURL string //支付 - 接受微信支付结果通知的接口地址
	PayKey       string //支付 - 商户后台设置的支付 key
	PayCertFile  string
	PayKeyFile   string
	PayP12File   string
}

type PayPrepareRequest struct {
	TotalFee   int32
	RemoteIp   string
	Body       string
	OutTradeNo string
	OpenID     string
	TradeType  string
}

type PayPrepareResponse struct {
	PrePayId  string `json:"pre_pay_id"`
	AppID     string `json:"app_id"`
	Timestamp int64  `json:"timestamp"`
	NonceStr  string `json:"noncestr"`
	Package   string `json:"package"`
	Sign      string `json:"sign"`
	SignType  string `json:"sign_type"`
}

type PayNativeRequest struct {
	Appid       string `xml:"appid"`
	IsSubscribe string `xml:"is_subscribe"`
	MchId       string `xml:"mch_id"`
	NonceStr    string `xml:"nonce_str"`
	Openid      string `xml:"openid"`
	ProductId   string `xml:"product_id"`
}

type PayTransfer struct {
	TransferType int
	BankNo       string
	BankCode     string
	Openid       string
	RealName     string
	Amount       int32
	TradeNo      string
	RemoteIp     string
	Desc         string
}

type PayCallback struct {
	Appid         string `xml:"appid"`
	BankType      string `xml:"bank_type"`
	CashFee       string `xml:"cash_fee"`
	FeeType       string `xml:"fee_type"`
	IsSubscribe   string `xml:"is_subscribe"`
	MchId         string `xml:"mch_id"`
	NonceStr      string `xml:"nonce_str"`
	Openid        string `xml:"openid"`
	OutTradeNo    string `xml:"out_trade_no"`
	Sign          string `xml:"sign"`
	TimeEnd       string `xml:"time_end"`
	TotalFee      string `xml:"total_fee"`
	TradeType     string `xml:"trade_type"`
	TransactionId string `xml:"transaction_id"`
}
type PayCallbackResponse struct {
	XMLName    xml.Name `xml:"xml"`
	ReturnCode string   `xml:"return_code"`
	ReturnMsg  string   `xml:"return_msg"`
}
