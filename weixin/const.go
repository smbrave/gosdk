package weixin

const (
	UrlWeappSendTemplete = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send"
	UrlWeappUniformSendTemplete = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/uniform_send"
	UrlWeappSend         = "https://api.weixin.qq.com/cgi-bin/message/custom/send"

	UrlQiyeToken  = "https://qyapi.weixin.qq.com/cgi-bin/gettoken"
	UrlQiyeSend   = "https://qyapi.weixin.qq.com/cgi-bin/message/send"
	UrlQiyeUpload = "https://qyapi.weixin.qq.com/cgi-bin/media/upload"

	UrlPayTransferWeixin = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers"
	UrlPayTransferBank   = "https://api.mch.weixin.qq.com/mmpaysptrans/pay_bank"
	UrlPayGetPublicKey   = "https://fraud.mch.weixin.qq.com/risk/getpublickey"
)

const (
	BodyTypeXml  = "application/xml; charset=utf-8"
	BodyTypeJson = "application/json; charset=utf-8"
)

const (
	TransferTypeWeixin = 1
	TransferTypeBank   = 2
)

const (
	MsgTypeText  = "text"
	MsgTypeImage = "image"
	MsgTypeLink  = "link"
	MsgTypePage  = "miniprogrampage"
)
