package weixin

import (
	"bytes"
	"crypto/tls"
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/panghu1024/anypay"
	"github.com/silenceper/wechat"
	"github.com/silenceper/wechat/cache"
	"github.com/silenceper/wechat/pay"
)

type WxPay struct {
	wepay     *pay.Pay
	anyPay    *anypay.WePay
	publicKey []byte
	tlsClient *http.Client
	stdClient *http.Client
}

func NewPay(cfg *PayConfig) *WxPay {
	c := new(wechat.Config)

	c.PayKey = cfg.PayKey
	c.PayMchID = cfg.PayMchID
	c.PayNotifyURL = cfg.PayNotifyURL
	c.AppSecret = cfg.AppSecret
	c.AppID = cfg.AppID
	c.Cache = cache.NewMemcache()
	wc := wechat.NewWechat(c)

	pay := &WxPay{
		wepay:     wc.GetPay(),
		stdClient: &http.Client{},
	}

	if cfg.PayCertFile != "" && cfg.PayKeyFile != "" {
		err := pay.withCert(cfg.PayCertFile, cfg.PayKeyFile)
		if err != nil {
			panic(err)
		}
	}

	publicKey, err := pay.GetPublicKey()
	if err != nil {
		panic(err)
	}

	pay.publicKey = publicKey

	//anypay 创建实例
	apy := anypay.NewWePay(anypay.WeConfig{
		AppId:       cfg.AppID,
		MchId:       cfg.PayMchID,
		Key:         cfg.PayKey,
		CertKeyPath: cfg.PayKeyFile,
		CertPemPath: cfg.PayCertFile,
		CertP12Path: cfg.PayP12File,
	})
	pay.anyPay = &apy

	return pay
}

// 发送请求
func (c *WxPay) post(url string, params Params, tls bool) (Params, error) {
	var httpc *http.Client
	if tls {
		if c.tlsClient == nil {
			return nil, errors.New("tls wepay is not initialized")
		}
		httpc = c.tlsClient
	} else {
		httpc = c.stdClient
	}
	buf := bytes.NewBuffer(params.Encode())
	resp, err := httpc.Post(url, BodyTypeXml, buf)
	if err != nil {
		return nil, err
	}
	result := NewParams()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	result.Decode(body)
	return result, nil
}

// 附着商户证书
func (c *WxPay) withCert(certFile, keyFile string) error {
	cert, err := ioutil.ReadFile(certFile)
	if err != nil {
		return err
	}
	key, err := ioutil.ReadFile(keyFile)
	if err != nil {
		return err
	}
	tlsCert, err := tls.X509KeyPair(cert, key)
	if err != nil {
		return err
	}

	conf := &tls.Config{
		Certificates: []tls.Certificate{tlsCert},
	}
	trans := &http.Transport{
		TLSClientConfig: conf,
	}

	c.tlsClient = &http.Client{
		Transport: trans,
	}

	return nil
}

func (c *WxPay) GetPublicKey() ([]byte, error) {

	params := NewParams()
	params.Set("mch_id", c.wepay.PayMchID)
	params.Set("nonce_str", RandomStr(32))
	params.Set("sign_type", "MD5")
	params.Set("sign", params.Sign(c.wepay.PayKey))

	result, err := c.post(UrlPayGetPublicKey, params, true)
	if err != nil {
		return nil, err
	}
	if result.GetString("return_code") != "SUCCESS" {
		return nil, errors.New(result.GetString("return_msg"))
	}

	if result.GetString("result_code") != "SUCCESS" {
		return nil, fmt.Errorf("%s:%s",
			result.GetString("err_code"), result.GetString("err_code_des"))
	}

	return []byte(result.GetString("pub_key")), nil
}

func (c *WxPay) PrepareJSAPI(prePayId string) (*PayPrepareResponse, error) {
	packageStr := "prepay_id=" + prePayId
	timestamp := time.Now().Unix()
	signType := "MD5"
	noceStr := RandomStr(32)
	payParams := NewParams()
	payParams.Set("appId", c.wepay.AppID)
	payParams.Set("nonceStr", noceStr)
	payParams.Set("timeStamp", timestamp)
	payParams.Set("package", packageStr)
	payParams.Set("signType", signType)

	rsp := new(PayPrepareResponse)
	rsp.PrePayId =prePayId
	rsp.Package = packageStr
	rsp.AppID = c.wepay.AppID
	rsp.Timestamp = timestamp
	rsp.NonceStr = noceStr
	rsp.SignType = signType
	rsp.Sign = payParams.Sign(c.wepay.PayKey)

	return rsp, nil
}

//统一下预支付订单
func (c *WxPay) PrepareOrder(prepareOrder *PayPrepareRequest) (*PayPrepareResponse, error) {
	params := new(pay.Params)
	params.OpenID = prepareOrder.OpenID
	params.TotalFee = fmt.Sprintf("%d", prepareOrder.TotalFee)
	params.Body = prepareOrder.Body
	params.CreateIP = prepareOrder.RemoteIp
	params.OutTradeNo = prepareOrder.OutTradeNo
	if prepareOrder.TradeType == "" {
		params.TradeType = "JSAPI"
	} else {
		params.TradeType = prepareOrder.TradeType
	}

	payOrder, err := c.wepay.PrePayOrder(params)
	if err != nil {
		return nil, err
	}
	if strings.ToUpper(payOrder.ResultCode) != "SUCCESS" {
		return nil, fmt.Errorf("%s", payOrder.ResultCode)
	}

	packageStr := "prepay_id=" + payOrder.PrePayID
	timestamp := time.Now().Unix()
	signType := "MD5"

	payParams := NewParams()
	payParams.Set("appId", payOrder.AppID)
	payParams.Set("nonceStr", payOrder.NonceStr)
	payParams.Set("timeStamp", timestamp)
	payParams.Set("package", packageStr)
	payParams.Set("signType", signType)

	rsp := new(PayPrepareResponse)
	rsp.PrePayId = payOrder.PrePayID
	rsp.Package = packageStr
	rsp.AppID = payOrder.AppID
	rsp.Timestamp = timestamp
	rsp.NonceStr = payOrder.NonceStr
	rsp.SignType = signType
	rsp.Sign = payParams.Sign(c.wepay.PayKey)

	return rsp, nil
}

// 计算NativeQrcocde支付的二维码内容
func (w *WxPay) NativeQrcode(productId string) string {
	params := NewParams()
	nonceStr := RandomStr(32)
	timestmp := time.Now().Unix()

	params.Set("appid", w.wepay.AppID)
	params.Set("mch_id", w.wepay.PayMchID)
	params.Set("nonce_str", nonceStr)
	params.Set("product_id", productId)
	params.Set("time_stamp", timestmp)
	sign := params.Sign(w.wepay.PayKey)

	return fmt.Sprintf("weixin://wxpay/bizpayurl?appid=%s&mch_id=%s&nonce_str=%s&product_id=%s&time_stamp=%d&sign=%s",
		w.wepay.AppID, w.wepay.PayMchID, nonceStr, productId, timestmp, sign)

}

//用户扫码之后的回调内容解析
func (c *WxPay) NativeQrcodeRequest(body []byte) (*PayNativeRequest, error) {
	params := NewParams()
	params.Decode(body)

	if params.CheckSign(c.wepay.PayKey) == false {
		return nil, errors.New("sign error")
	}

	var req PayNativeRequest

	req.Appid = params.GetString("openid")
	req.MchId = params.GetString("mch_id")
	req.IsSubscribe = params.GetString("is_subscribe")
	req.ProductId = params.GetString("product_id")
	req.Openid = params.GetString("openid")

	return &req, nil
}

//用户扫码之后的回调内容应答
func (c *WxPay) NativeQrcodeResonse(prepareId string) []byte {
	params := NewParams()
	params.Set("return_code", "SUCCESS")
	params.Set("return_msg", "SUCCESS")
	params.Set("appid", c.wepay.AppID)
	params.Set("mch_id", c.wepay.PayMchID)
	params.Set("nonce_str", RandomStr(32))
	params.Set("prepay_id", prepareId)
	params.Set("result_code", "SUCCESS")
	params.Set("sign", params.Sign(c.wepay.PayKey))

	return params.Encode()
}

//企业付款
func (c *WxPay) Transfer(transfer *PayTransfer) error {

	params := NewParams()

	params.Set("nonce_str", RandomStr(32))
	params.Set("partner_trade_no", transfer.TradeNo)
	params.Set("amount", transfer.Amount)
	params.Set("desc", transfer.Desc)

	//付款到微信
	if transfer.TransferType == TransferTypeWeixin {
		params.Set("mchid", c.wepay.PayMchID)
		params.Set("mch_appid", c.wepay.AppID)
		params.Set("openid", transfer.Openid)
		if transfer.RealName != "" {
			params.Set("check_name", "FORCE_CHECK")
			params.Set("re_user_name", transfer.RealName)
		} else {
			params.Set("check_name", "NO_CHECK")
		}
		params.Set("spbill_create_ip", transfer.RemoteIp)

		//付款到银行卡
	} else if transfer.TransferType == TransferTypeBank {
		fmt.Println(string(c.publicKey), "name:", transfer.RealName, "bankno:", transfer.BankNo)
		bankNo, err := RsaEncrypt([]byte(transfer.BankNo), c.publicKey)
		if err != nil {
			return err
		}
		realName, err := RsaEncrypt([]byte(transfer.RealName), c.publicKey)
		if err != nil {
			return err
		}

		params.Set("mch_id", c.wepay.PayMchID)
		params.Set("enc_bank_no", base64.StdEncoding.EncodeToString(bankNo))
		params.Set("enc_true_name", base64.StdEncoding.EncodeToString(realName))
		params.Set("bank_code", transfer.BankCode)

	} else {
		return errors.New("transfer_type error")
	}

	url := UrlPayTransferWeixin
	if transfer.TransferType == TransferTypeBank {
		url = UrlPayTransferBank
	}
	params.Set("sign", params.Sign(c.wepay.PayKey))
	result, err := c.post(url, params, true)
	if err != nil {
		return err
	}

	if result.GetString("return_code") != "SUCCESS" {
		return errors.New(result.GetString("return_msg"))
	}

	if result.GetString("result_code") != "SUCCESS" {
		return fmt.Errorf("%s:%s",
			result.GetString("err_code"), result.GetString("err_code_des"))
	}
	return nil
}

func (c *WxPay) PayCallback(body []byte) (*PayCallback, error) {
	params := NewParams()
	params.Decode(body)

	if params.GetString("return_code") != "SUCCESS" {
		return nil, fmt.Errorf("return_code:%s return_msg:%s",
			params.GetString("return_code"), params.GetString("return_msg"))
	}

	if !params.CheckSign(c.wepay.PayKey) {
		return nil, errors.New("sign failed")
	}

	var payCallback PayCallback

	payCallback.Appid = params.GetString("appid")
	payCallback.BankType = params.GetString("bank_type")
	payCallback.CashFee = params.GetString("cash_fee")
	payCallback.FeeType = params.GetString("fee_type")
	payCallback.IsSubscribe = params.GetString("is_subscribe")
	payCallback.MchId = params.GetString("mch_id")
	payCallback.Openid = params.GetString("openid")
	payCallback.OutTradeNo = params.GetString("out_trade_no")
	payCallback.TimeEnd = params.GetString("time_end")
	payCallback.TotalFee = params.GetString("total_fee")
	payCallback.TradeType = params.GetString("trade_type")
	payCallback.TransactionId = params.GetString("transaction_id")

	return &payCallback, nil
}
