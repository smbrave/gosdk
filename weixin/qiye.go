package weixin

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"os/exec"
	"time"

	"github.com/astaxie/beego/httplib"
)

///===================================================================
type WxQiye struct {
	corpid      string
	secret      string
	sender      string
	tokenUpdate int64
	token       string
}

func NewQiye(cfg *QiyeConfig) *WxQiye {
	return &WxQiye{
		corpid:      cfg.Corpid,
		secret:      cfg.Secret,
		sender:      cfg.Sender,
		tokenUpdate: 0,
	}
}

func (c *WxQiye) getToken() (string, error) {

	if time.Now().Unix()-c.tokenUpdate <= 3600 {
		return c.token, nil
	}

	url := fmt.Sprintf("%s?corpid=%s&corpsecret=%s",
		UrlQiyeToken, c.corpid, c.secret)

	res, err := httplib.Get(url).String()
	if err != nil {
		return "", err
	}

	var obj QiyeToken
	err = json.Unmarshal([]byte(res), &obj)
	if err != nil {
		return "", err
	}

	if obj.ExpiresIn != 0 {
		c.token = obj.AccessToken
		return c.token, nil
	} else {
		return "", errors.New(string(res))
	}

}

func (c *WxQiye) SendText(receiver, content string) error {

	token, err := c.getToken()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s?access_token=%s", UrlQiyeSend, token)
	req := new(QiyeTextRequest)
	req.MstType = "text"
	req.AgentId = c.sender
	req.Text.Content = content
	req.ToUser = receiver

	data, _ := json.Marshal(req)
	result, err := httplib.Post(url).Body(data).String()
	if err != nil {
		return err
	}

	var rsp QiyeResponse
	err = json.Unmarshal([]byte(result), &rsp)
	if err != nil {
		return err
	}

	if rsp.ErrCode != 0 {
		return fmt.Errorf("%d:%s", rsp.ErrCode, rsp.ErrMsg)
	}
	return nil
}

func (c *WxQiye) SendImage(receiver, meidiaId string) error {
	token, err := c.getToken()
	if err != nil {
		return err
	}

	url := fmt.Sprintf("%s?access_token=%s", UrlQiyeSend, token)
	req := new(QiyeImageRequest)
	req.MstType = "image"
	req.AgentId = c.sender
	req.ToUser = receiver
	req.Image.MediaID = meidiaId

	data, _ := json.Marshal(req)
	result, err := httplib.Post(url).Body(data).String()
	if err != nil {
		return err
	}

	var rsp QiyeResponse
	err = json.Unmarshal([]byte(result), &rsp)
	if err != nil {
		return err
	}
	if rsp.ErrCode != 0 {
		return fmt.Errorf("%d:%s", rsp.ErrCode, rsp.ErrMsg)
	}

	return nil
}

func (c *WxQiye) Upload(path string) (string, error) {
	token, err := c.getToken()
	if err != nil {
		return "", err
	}

	url := fmt.Sprintf("%s?access_token=%s&type=image", UrlQiyeUpload, token)
	var out bytes.Buffer
	cmd := exec.Command("curl", url, "-F", fmt.Sprintf("filename=@%s", path))
	cmd.Stdout = &out
	err = cmd.Start()
	if err != nil {
		return "", err
	}

	err = cmd.Wait()
	if err != nil {
		return "", err
	}

	var rsp QiyeUploadResponse
	if err = json.Unmarshal(out.Bytes(), &rsp); err != nil {
		return "", err
	}
	if rsp.ErrCode != 0 {
		return "", fmt.Errorf("%d:%s", rsp.ErrCode, rsp.ErrMsg)
	}

	return rsp.MediaId, nil
}

func (c *WxQiye) SendCard(receiver, title, description, direct string, btn []*QiyeCardBtn) error {
	token, err := c.getToken()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s?access_token=%s", UrlQiyeSend, token)

	var req QiyeCardRequest
	req.ToUser = receiver
	req.AgentId = c.sender
	req.MstType = "taskcard"
	req.TaskCard.TaskId = fmt.Sprintf("%d_%d", time.Now().Unix(), time.Now().Nanosecond())
	req.TaskCard.Description = description
	req.TaskCard.Url = direct
	req.TaskCard.Title = title
	req.TaskCard.Btn = btn

	data, _ := json.Marshal(req)
	result, err := httplib.Post(url).Body(data).String()
	if err != nil {
		return err
	}

	var rsp QiyeResponse
	err = json.Unmarshal([]byte(result), &rsp)
	if err != nil {
		return err
	}
	if rsp.ErrCode != 0 {
		return fmt.Errorf("%d:%s", rsp.ErrCode, rsp.ErrMsg)
	}
	return nil
}
