package weixin

import (
	"encoding/json"
	"fmt"

	"github.com/silenceper/wechat"
	"github.com/silenceper/wechat/cache"
	"github.com/silenceper/wechat/miniprogram"
	"gitlab.com/jiangyong27/gobase/httpclient"
)

type Weapp struct {
	client *miniprogram.MiniProgram
}

func NewWeapp(cfg *WeappConfig) *Weapp {
	c := new(wechat.Config)
	c.AppID = cfg.AppID
	c.AppSecret = cfg.AppSecret
	c.Cache = cache.NewMemory()
	wc := wechat.NewWechat(c)
	mp := wc.GetMiniProgram()
	return &Weapp{
		client: mp,
	}
}

func (c *Weapp) GetAppid() string {
	return c.client.AppID
}
func (c *Weapp) Code2Session(code string) (*WeappJsSession, error) {
	var rsp WeappJsSession
	res, err := c.client.Code2Session(code)
	if err != nil {
		return nil, err
	}
	if res.ErrCode != 0 {
		return nil, fmt.Errorf("%d:%s", res.ErrCode, res.ErrMsg)
	}

	rsp.OpenId = res.OpenID
	rsp.SessionKey = res.SessionKey
	rsp.UnionId = res.UnionID
	return &rsp, nil
}

func (c *Weapp) GetUserPhone(sessionKey, encryptedData, iv string) (string, error) {
	userInfo, err := c.client.Decrypt(sessionKey, encryptedData, iv)
	if err != nil {
		return "", err
	}
	//return userInfo.PurePhoneNumber, nil
	return userInfo.NickName, nil
}

func (c *Weapp) GetOrderQrCode(page, sence string) ([]byte, error) {
	var param miniprogram.QRCoder
	param.Scene = sence
	param.Page = page
	return c.client.GetWXACodeUnlimit(param)
}

func (c *Weapp) SendTemplete(req *WeappTempleteRequest) error {
	accessToken, err := c.client.GetAccessToken()
	if err != nil {
		return err
	}

	message := new(weapp_uniform_template_t)
	message.Touser = req.Touser
	message.WeappMsg = new(weapp_template_msg_t)
	message.WeappMsg.TemplateId = req.WeappTemplateMsg.TemplateId
	message.WeappMsg.FormId = req.WeappTemplateMsg.FormId
	message.WeappMsg.Page = req.WeappTemplateMsg.Page
	message.WeappMsg.Data = make(map[string]*weapp_template_data_t)
	for i, dat := range req.WeappTemplateMsg.Data {
		item := new(weapp_template_data_t)
		item.Value = dat
		message.WeappMsg.Data[fmt.Sprintf("keyword%d", i+1)] = item
	}

	// todo优化公众号模板消息
	message.MpMsg = new(weapp_mp_template_msg_t)
	message.MpMsg.TemplateId = req.MpTemplateMsg.TemplateId
	message.MpMsg.Url = req.MpTemplateMsg.Url
	message.MpMsg.Appid = req.MpTemplateMsg.Appid
	message.MpMsg.Miniprogram.Appid = req.MpTemplateMsg.Miniprogram.Appid
	message.MpMsg.Miniprogram.Pagepath = req.MpTemplateMsg.Miniprogram.Pagepath
	for i, dat := range req.MpTemplateMsg.Data {
		item := new(weapp_template_data_t)
		item.Value = dat
		message.MpMsg.Data[fmt.Sprintf("keyword%d", i+1)] = item
	}

	reqBody, _ := json.Marshal(message)
	fmt.Println(string(reqBody))
	hc := httpclient.Post(fmt.Sprintf("%s?access_token=%s", UrlWeappUniformSendTemplete, accessToken))
	res, err := hc.ByteBody(reqBody).ToByte()
	if err != nil {
		return err
	}
	var rsp WeappBaseResonse
	err = json.Unmarshal(res, &rsp)
	if err != nil {
		return err
	}

	if rsp.ErrCode != 0 {
		return fmt.Errorf("%d:%s", rsp.ErrCode, rsp.ErrMsg)
	}
	return nil
}

func (c *Weapp) SendCustomText(user, content string) error {
	accessToken, err := c.client.GetAccessToken()
	if err != nil {
		return err
	}
	var req WeappCustomRequest
	req.Text.Content = content
	req.Touser = user
	req.MsgType = MsgTypeText

	reqBody, _ := json.Marshal(req)
	hc := httpclient.Post(fmt.Sprintf("%s?access_token=%s", UrlWeappSend, accessToken))
	res, err := hc.ByteBody(reqBody).ToByte()
	if err != nil {
		return err
	}
	var rsp WeappBaseResonse
	err = json.Unmarshal(res, &rsp)
	if err != nil {
		return err
	}

	if rsp.ErrCode != 0 {
		return fmt.Errorf("%d:%s", rsp.ErrCode, rsp.ErrMsg)
	}
	return nil
}

func (c *Weapp) SendCustom(req *WeappCustomRequest) error {
	accessToken, err := c.client.GetAccessToken()
	if err != nil {
		return err
	}

	reqBody, _ := json.Marshal(req)
	hc := httpclient.Post(fmt.Sprintf("%s?access_token=%s", UrlWeappSend, accessToken))
	res, err := hc.ByteBody(reqBody).ToByte()
	if err != nil {
		return err
	}
	var rsp WeappBaseResonse
	err = json.Unmarshal(res, &rsp)
	if err != nil {
		return err
	}

	if rsp.ErrCode != 0 {
		return fmt.Errorf("%d:%s", rsp.ErrCode, rsp.ErrMsg)
	}
	return nil
}
