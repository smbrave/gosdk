module gitlab.com/smbrave/gosdk

go 1.13

require (
	github.com/astaxie/beego v1.12.0
	github.com/panghu1024/anypay v0.0.0-20191126100335-6426d4a32c1c
	github.com/silenceper/wechat v1.2.3
	gitlab.com/jiangyong27/gobase v0.0.0-20190803090631-636904af04b9
)
